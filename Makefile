DOTNET := dotnet
TESTDIR := tests

## BUILD TARGETS ##
all: build test
build:
	$(DOTNET) build
test:
	$(DOTNET) test $(TESTDIR)/*
clean: 
	$(DOTNET) clean